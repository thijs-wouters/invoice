Feature: Create order for a business customer
  As a business customer
  In order to start ordering something
  I want to create an order

  Input:
  customer_reference => String
  Output:
  order_reference => String

  Scenario: Create order for known customer
    Given it is 1/1/2013
    And a customer with reference "12" exists
    When I create an order for customer "12"
    Then a new order is created with reference "2013-1"
    And order "2013-1" is an order for customer "12"
    And order "2013-1" does not have a date
    And order "2013-1" does not have any order lines
    And the presenter should present a reference of "2013-1"

  Scenario: Create order for an unknown customer
    Given a customer with reference "12" does not exist
    When I create an order for customer "12"
    Then I should receive an error stating "customer does not exist"
