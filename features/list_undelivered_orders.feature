Feature: List undelivered orders
  As a deliverer
  In order to get an overview of the work I need to do
  I want to list all the orders that are not yet delivered

  Input:
  Output:
    Array of hashes
      reference => String
      order_date => Date
      delivery_date => Date
      lines => Array of Hashes
        reference => String
        amount => Integer
      customer => Hash
        reference => String
        delivery_address => String

  Scenario: Happy path
    Given there are 5 orders with delivery date
    And there are 5 orders without delivery date
    When I request the list of undelivered orders
    Then I receive the 5 orders without delivery date