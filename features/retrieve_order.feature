Feature: Retrieve order
  As a deliverer
  I want to get the details of an order
  So that I know what and where I need to deliver

  Input:
    reference => String
  Output:
    reference => String
    order_date => Date
    delivery_date => Date
    lines => Array of Hashes
      reference => String
      amount => Integer
    customer => Hash
      reference => String
      delivery_address => String

  Scenario: Happy path
    Given an order with reference 12 is in the repository
    When I request the details of order 12
    Then I receive the order reference of 12
    And I receive the order date
    And I receive the delivery date
    And I receive the order lines
    And I receive the customer

  Scenario: Order with given reference does not exist
    Given that order with reference 12 does not exist
    When I request the details of order 12
    Then I receive the order reference of 12
    And I should receive an error stating "order does not exist"