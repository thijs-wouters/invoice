Given(/^there are (\d+) orders with delivery date$/) do |number|
  old_order_count = order_gateway.count
  number.times do
    order_gateway.create(FactoryGirl.build(:order, :delivery_date => Date.today))
  end
  expect(order_gateway.count).to eq(old_order_count + number)
end

Given(/^there are (\d+) orders without delivery date$/) do |number|
  old_order_count = order_gateway.count
  number.times do
    order_gateway.create(FactoryGirl.build(:order, :delivery_date => nil))
  end
  expect(order_gateway.count).to eq(old_order_count + number)
end

Given(/^that order with reference (\w+) does not exist$/) do |reference|
  order_gateway.delete(reference)
end

Given(/^an order with reference (\w+) is in the repository$/) do |reference|
  @order = FactoryGirl.build(:order, :reference => reference)
  order_gateway.create(@order)
  order_gateway.retrieve(reference).should == @order
end

When(/^I request the details of order (\w+)$/) do |reference|
  order_interactor.get_details(:reference => reference)
  mock_single_order_presenter.result.should_not be_nil
end

When(/^I request the list of undelivered orders$/) do
  ListUndeliveredOrders.new(mock_multi_order_presenter).execute({})
end

Then(/^I receive the order date$/) do
  mock_single_order_presenter.order_date.should == @order.date
end

Then(/^I receive the delivery date$/) do
  mock_single_order_presenter.delivery_date.should == @order.delivery_date
end

Then(/^I receive the order lines$/) do
  mock_single_order_presenter.order_lines.should == @order.lines.collect { |line| line.to_hash }
end

Then(/^I receive the customer$/) do
  mock_single_order_presenter.customer.should == @order.customer.to_hash
end

Then(/^I should receive an error stating "([^"]*)"$/) do |error|
  mock_single_order_presenter.error.should == error
end

Then(/^I receive the order reference of (\w+)$/) do |reference|
  mock_single_order_presenter.reference.should == reference
end

Then(/^I receive the (\d+) orders without delivery date$/) do |number|
  expect(mock_multi_order_presenter).to have(number).orders
  expect(mock_multi_order_presenter.orders.collect { |order| order.delivery_date }).to_not include(Date.today)
end

When(/^I create an order for customer "(.*?)"$/) do |customer_reference|
  CreateOrder.new(mock_single_order_presenter).execute(:customer => {:reference => customer_reference})
end

Then(/^a new order is created with reference "(.*?)"$/) do |reference|
  expect(order_gateway.retrieve(reference)).to_not be_nil
  expect(order_gateway.retrieve(reference)).to be_instance_of Order
end

And(/^order "(.*?)" is an order for customer "(.*?)"$/) do |reference, customer_reference|
  expect(order_gateway.retrieve(reference).customer).to be customer_gateway.retrieve(customer_reference)
end

And(/^order "(.*?)" does not have a date$/) do |reference|
  expect(order_gateway.retrieve(reference).date).to be_nil
end

And(/^order "(.*?)" does not have any order lines$/) do |reference|
  expect(order_gateway.retrieve(reference).lines).to be_empty
end

When(/^the presenter should present a reference of "([^"]*)"$/) do |reference|
  expect(mock_single_order_presenter.reference).to eq reference
end