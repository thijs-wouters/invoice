DATE = Transform(/^(\d+)\/(\d+)\/(\d+)$/) do |day, month, year|
  Date.new(year.to_i, month.to_i, day.to_i)
end

Given(/^it is (#{DATE})$/) do |date|
  Timecop.freeze(date)
  expect(Date.today).to eq date
end

And(/^a customer with reference "(.*?)" exists$/) do |reference|
  customer = FactoryGirl.build(:customer, :reference => reference)
  customer_gateway.create(customer)
  expect(customer_gateway.retrieve(reference)).to eq customer
end

Given(/^a customer with reference "([^"]*)" does not exist$/) do |reference|
  customer_gateway.delete reference
  expect(customer_gateway.retrieve(reference)).to be_kind_of NilCustomer
end