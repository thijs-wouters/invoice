Transform /^\d+$/ do |number_string|
  number_string.to_i
end