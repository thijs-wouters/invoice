module Mock
  class OrderGateway
    def initialize
      @orders = {}
    end

    def create order
      @orders[order.reference] = order
    end

    def retrieve reference
      @orders[reference] || NilOrder.new(reference)
    end

    def delete reference
      @orders.delete reference
    end

    def count
      @orders.count
    end

    def find options
      OrderList.new(@orders.values.select do |order|
        result = true
        options.each do |method, expected_value|
          result &&= order.send(method) == expected_value
        end
        result
      end)
    end

    def clear
      @orders = {}
    end
  end

  class CustomerGateway
    def create customer
      @customer = customer
    end

    def retrieve _
      @customer
    end

    def delete reference
      @customer = NilCustomer.new(reference)
    end
  end

  class SingleOrderPresenter
    attr_reader :result

    def present order
      @result = order
    end

    def reference
      @result[:reference]
    end

    def order_date
      @result[:date]
    end

    def delivery_date
      @result[:delivery_date]
    end

    def order_lines
      @result[:lines]
    end

    def customer
      @result[:customer]
    end

    def error
      @result[:error]
    end
  end

  class MultiOrderPresenter
    def orders
      @orders.collect do |order_hash|
        single_order_presenter = SingleOrderPresenter.new()
        single_order_presenter.present(order_hash)
        single_order_presenter
      end
    end

    def present orders
      @orders = orders
    end
  end
end

module KnowsOrders
  def mock_single_order_presenter
    @single_order_presenter ||= Mock::SingleOrderPresenter.new
  end

  def order_interactor
    RetrieveOrder.new(mock_single_order_presenter)
  end

  def mock_multi_order_presenter
    @mock_multi_order_presenter ||= Mock::MultiOrderPresenter.new
  end
end

World(KnowsOrders)

ORDER_GATEWAY = Mock::OrderGateway.new
CUSTOMER_GATEWAY = Mock::CustomerGateway.new

def order_gateway
  ORDER_GATEWAY
end

def customer_gateway
  CUSTOMER_GATEWAY
end
