class Customer
  attr_reader :reference, :delivery_address

  def initialize hash
    @reference = hash[:reference]
    @delivery_address = hash[:delivery_address]
  end

  def to_hash
    {
        :reference => reference,
        :delivery_address => delivery_address,
    }
  end

  def create_order reference
    Order.new({:reference => reference, :customer => self})
  end
end