class NilOrder < Order
  attr_reader :error

  def initialize reference, error = 'order does not exist'
    @reference = reference
    @error = error
  end

  def valid?
    false
  end

  private

  def lines_hash
    lines
  end

  def customer_hash
    customer
  end
end