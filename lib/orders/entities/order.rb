class Order
  attr_reader :reference, :date, :delivery_date, :lines, :customer, :error

  def initialize hash
    @reference = hash[:reference]
    @date = hash[:date]
    @delivery_date = hash[:delivery_date]
    @lines = hash[:lines] || []
    @customer = hash[:customer]
  end

  def to_hash
    {
        :reference => reference,
        :date => date,
        :error => error,
        :delivery_date => delivery_date,
        :lines => lines_hash(),
        :customer => customer_hash()
    }
  end

  def valid?
    true
  end

  private
  def customer_hash
    customer.to_hash
  end

  def lines_hash
    lines.collect { |line| line.to_hash }
  end

end