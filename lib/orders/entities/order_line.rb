class OrderLine
  attr_reader :product_reference, :amount

  def initialize hash
    @product_reference = hash[:product_reference]
    @amount = hash[:amount]
  end

  def to_hash
    {
        :product_reference => product_reference,
        :amount => amount,
    }
  end
end