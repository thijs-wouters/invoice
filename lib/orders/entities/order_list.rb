class OrderList
  attr_reader :orders

  def initialize orders
    @orders = orders
  end

  def to_hash
    @orders.collect { |order| order.to_hash }
  end
end