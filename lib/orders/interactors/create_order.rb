class CreateOrder
  def initialize order_presenter
    @order_presenter = order_presenter
  end

  def execute request
    customer = customer_gateway.retrieve(request[:customer][:reference])
    new_order = customer.create_order(new_reference)
    order_gateway.create(new_order) if new_order.valid?
    @order_presenter.present(new_order.to_hash)
  end

  private
  def new_reference
    "#{Date.today.year}-#{order_gateway.count + 1}"
  end
end