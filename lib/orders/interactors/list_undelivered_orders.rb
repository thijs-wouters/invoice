class ListUndeliveredOrders
  def initialize multi_order_presenter
    @multi_order_presenter = multi_order_presenter
  end

  def execute _
    orders = order_gateway.find(:delivery_date => nil)
    @multi_order_presenter.present(orders.to_hash)
  end
end