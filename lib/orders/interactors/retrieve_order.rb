class RetrieveOrder
  def initialize single_order_presenter
    @single_order_presenter = single_order_presenter
  end

  def get_details request
    order = order_gateway.retrieve(request[:reference])
    @single_order_presenter.present(order.to_hash)
  end
end