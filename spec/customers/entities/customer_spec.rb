require 'spec_helper'

describe Customer do
  subject { Customer.new(
      :reference => 12,
      :delivery_address => 'Test 123',
  ) }

  describe 'new customer from hash' do
    it 'has the reference given in the hash' do
      subject.reference.should == 12
    end

    it 'has the delivery address given in the hash' do
      subject.delivery_address.should == 'Test 123'
    end

    describe '#to_hash' do
      it 'returns the customer as a hash' do
        subject.to_hash.should == {
            :reference => 12,
            :delivery_address => 'Test 123',
        }
      end
    end
  end

  describe '#create_order' do
    let (:reference) { 'ref' }

    it 'returns an order' do
      expect(subject.create_order(reference)).to be_an Order
    end

    it 'returns an order with the given reference' do
      expect(subject.create_order(reference).reference).to eq reference
    end

    it 'returns an order with self as customer' do
      expect(subject.create_order(reference).customer).to eq subject
    end

    it 'returns an order without any order lines' do
      expect(subject.create_order(reference).lines).to be_empty
    end

    it 'returns an order without order date' do
      expect(subject.create_order(reference).date).to be_nil
    end

    it 'returns an order without delivery date' do
      expect(subject.create_order(reference).delivery_date).to be_nil
    end
  end
end