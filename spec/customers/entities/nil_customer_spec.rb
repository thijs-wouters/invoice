require 'spec_helper'

describe NilCustomer do
  subject { NilCustomer.new(12) }

  describe '#create_order' do
    let(:new_reference) { "" }

    it 'returns a NilOrder' do
      expect(subject.create_order(new_reference)).to be_a NilOrder
    end

    it 'returns a NilOrder with the given reference' do
      expect(subject.create_order(new_reference).reference).to eq ''
    end

    it 'returns a NilOrder with the correct error message' do
      expect(subject.create_order(new_reference).error).to eq 'customer does not exist'
    end
  end
end