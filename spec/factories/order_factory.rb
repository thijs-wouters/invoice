FactoryGirl.define do
  factory :order do
    sequence :reference
    date Date.new(2013, 1, 1)
    delivery_date Date.new(2013, 2, 2)
    lines { [
        FactoryGirl.build(:order_line),
        FactoryGirl.build(:order_line)
    ] }
    customer
  end

  factory :order_line do
    product_reference 12
    amount 5
  end

  factory :customer do
    reference 12
    delivery_address 'Test 123'
  end
end