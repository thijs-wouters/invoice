require 'spec_helper'

describe NilOrder do
  describe 'create with reference' do
    subject { NilOrder.new(12) }

    it 'has the given reference' do
      subject.reference.should == 12
    end

    it 'has an error that the order do not exist' do
      subject.error.should == 'order does not exist'
    end

    it 'has no date' do
      subject.date.should be_nil
    end

    it 'has no delivery date' do
      subject.delivery_date.should be_nil
    end

    it 'has no customer' do
      subject.customer.should be_nil
    end

    it 'has no order lines' do
      subject.lines.should be_nil
    end

    describe '#to_hash' do
      it 'returns the hash' do
        subject.to_hash.should == {
            :reference => 12,
            :error => 'order does not exist',
            :date => nil,
            :delivery_date => nil,
            :customer => nil,
            :lines => nil,
        }
      end
    end

    describe '#valid?' do
      it 'returns false' do
        expect(subject.valid?).to be_false
      end
    end
  end
end