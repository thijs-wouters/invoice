require 'spec_helper'

describe OrderLine do
  describe 'new order line from hash' do
    subject { OrderLine.new(
        :product_reference => 12,
        :amount => 5,
    ) }

    it 'should have the reference specified' do
      subject.product_reference.should == 12
    end

    it 'should have the amount specified' do
      subject.amount.should == 5
    end

    describe '#to_hash' do
      it 'returns the order line as a hash' do
        subject.to_hash.should == {
            :product_reference => 12,
            :amount => 5,
        }
      end
    end
  end
end