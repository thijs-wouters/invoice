require 'spec_helper'

describe OrderList do
  let(:orders) { FactoryGirl.build_list(:order, 2) }
  subject { OrderList.new(orders) }

  describe '.new' do
    it 'sets the orders that were given' do
      expect(subject.orders).to be orders
    end
  end

  describe '#to_hash' do
    context 'no orders' do
      let(:orders) { [] }
      it 'returns an empty list' do
        expect(subject.to_hash).to be_empty
      end
    end

    context 'one order' do
      let(:orders) { FactoryGirl.build_list(:order, 1) }

      it 'returns a list of that order' do
        expect(subject.to_hash).to include(orders[0].to_hash)
      end
    end

    context 'two orders' do
      let(:orders) { FactoryGirl.build_list(:order, 2) }

      it 'returns an array of the hashes of its orders' do
        expect(subject.to_hash).to include(orders[0].to_hash)
        expect(subject.to_hash).to include(orders[1].to_hash)
      end
    end
  end
end