require 'spec_helper'

describe Order do
  describe 'new order from hash without lines' do
    subject {
      Order.new(
          :reference => 12,
          :date => Date.new(2013, 1, 1),
          :delivery_date => Date.new(2013, 2, 2),
          :customer => FactoryGirl.build(:customer),
      ) }

    it 'has the reference given in the hash' do
      subject.reference.should == 12
    end

    it 'has the date given in the hash' do
      subject.date.should == Date.new(2013, 1, 1)
    end

    it 'has the delivery date given in the hash' do
      subject.delivery_date.should == Date.new(2013, 2, 2)
    end

    it 'has the customer given in the hash' do
      subject.customer.should be_an_instance_of Customer
    end

    it 'should have no lines' do
      subject.should have(0).lines
    end

    it 'has no error' do
      subject.error.should be_nil
    end

    describe '#to_hash' do
      it 'returns the order as a hash' do
        subject.to_hash.should == {
            :reference => 12,
            :date => Date.new(2013, 1, 1),
            :delivery_date => Date.new(2013, 2, 2),
            :lines => [],
            :customer => FactoryGirl.attributes_for(:customer),
            :error => nil,
        }
      end
    end
  end

  describe 'new order from hash with one line' do
    subject {
      Order.new(
          :reference => 12,
          :date => Date.new(2013, 1, 1),
          :delivery_date => Date.new(2013, 2, 2),
          :lines => [FactoryGirl.build(:order_line)],
          :customer => FactoryGirl.build(:customer),
      ) }

    it 'has the reference given in the hash' do
      subject.reference.should == 12
    end

    it 'has the date given in the hash' do
      subject.date.should == Date.new(2013, 1, 1)
    end

    it 'has the delivery date given in the hash' do
      subject.delivery_date.should == Date.new(2013, 2, 2)
    end

    it 'has the lines one line' do
      subject.should have(1).lines
      subject.lines[0].should be_an_instance_of OrderLine
    end

    it 'has the customer given in the hash' do
      subject.customer.should be_an_instance_of Customer
    end

    it 'has no error' do
      subject.error.should be_nil
    end

    describe 'to_hash' do
      it 'returns the order as a hash' do
        subject.to_hash.should == {
            :reference => 12,
            :date => Date.new(2013, 1, 1),
            :delivery_date => Date.new(2013, 2, 2),
            :lines => [FactoryGirl.attributes_for(:order_line)],
            :customer => FactoryGirl.attributes_for(:customer),
            :error => nil,
        }
      end
    end
  end

  describe 'new order from hash with two lines' do
    subject {
      Order.new(
          :reference => 12,
          :date => Date.new(2013, 1, 1),
          :delivery_date => Date.new(2013, 2, 2),
          :lines => [
              FactoryGirl.build(:order_line),
              FactoryGirl.build(:order_line)
          ],
          :customer => FactoryGirl.build(:customer),
      ) }

    it 'has the reference given in the hash' do
      subject.reference.should == 12
    end

    it 'has the date given in the hash' do
      subject.date.should == Date.new(2013, 1, 1)
    end

    it 'has the delivery date given in the hash' do
      subject.delivery_date.should == Date.new(2013, 2, 2)
    end

    it 'has the customer given in the hash' do
      subject.customer.should be_an_instance_of Customer
    end

    it 'should have an two lines' do
      subject.should have(2).lines
    end

    it 'has no error' do
      subject.error.should be_nil
    end

    describe '#to_hash' do
      it 'returns the order as a hash' do
        subject.to_hash.should == {
            :reference => 12,
            :date => Date.new(2013, 1, 1),
            :delivery_date => Date.new(2013, 2, 2),
            :lines => [
                FactoryGirl.attributes_for(:order_line),
                FactoryGirl.attributes_for(:order_line),
            ],
            :customer => FactoryGirl.attributes_for(:customer),
            :error => nil,
        }
      end
    end

    describe '#valid?' do
      it 'returns true' do
        expect(subject.valid?).to be_true
      end
    end
  end
end