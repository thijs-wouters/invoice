require 'spec_helper'

describe CreateOrder do
  subject { CreateOrder.new(order_presenter) }

  let(:order_presenter) { double(:order_presenter, :present => nil) }
  let(:order_gateway) { double(:order_gateway, :create => nil, :count => 0) }
  let(:customer_gateway) { double(:customer_gateway, :retrieve => customer) }
  let(:customer) { double(:customer, :create_order => order) }
  let(:order) { double(:order, :valid? => true, :to_hash => order_hash) }
  let(:order_hash) { {} }

  before do
    allow(subject).to receive(:order_gateway).and_return(order_gateway)
    allow(subject).to receive(:customer_gateway).and_return(customer_gateway)
  end

  describe '#execute' do
    it 'should lookup the customer' do
      expect(customer_gateway).to receive(:retrieve).with(12)
      subject.execute(:customer => {:reference => 12})
    end

    it 'should create a new order with the given customer' do
      expect(customer).to receive(:create_order).with(anything).and_return(order)
      subject.execute(:customer => {:reference => 12})
    end

    it 'should present the new order' do
      expect(order_presenter).to receive(:present).with(order_hash)
      subject.execute(:customer => {:reference => 12})
    end

    context 'the order is valid' do
      before do
        allow(order).to receive(:valid?).and_return(true)
      end

      it 'saves the order' do
        expect(order_gateway).to receive(:create).with(order)
        subject.execute(:customer => {:reference => 12})
      end
    end

    context 'the order is not valid' do
      before do
        allow(order).to receive(:valid?).and_return(false)
      end

      it 'does not save the order' do
        expect(order_gateway).to_not receive(:create).with(order)
        subject.execute(:customer => {:reference => 12})
      end
    end

    context 'it is 2013' do
      before do
        Timecop.freeze(Date.new(2013, 1, 1))
      end

      context 'the first order this year' do
        it 'should create the new order with reference 2013-1' do
          expect(customer).to receive(:create_order).with('2013-1')
          subject.execute(:customer => {:reference => 12})
        end
      end

      context 'the second order this year' do
        before do
          allow(order_gateway).to receive(:count).and_return(1)
        end

        it 'should create the new order with reference 2013-2' do
          expect(customer).to receive(:create_order).with('2013-2')
          subject.execute(:customer => {:reference => 12})
        end
      end
    end

    context 'it is 2014' do
      before do
        Timecop.freeze(Date.new(2014, 1, 1))
      end

      it 'should create the new order with reference 2014-1' do
        expect(customer).to receive(:create_order).with('2014-1')
        subject.execute(:customer => {:reference => 12})
      end
    end
  end
end