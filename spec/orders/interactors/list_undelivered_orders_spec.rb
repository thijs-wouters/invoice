require 'spec_helper'

describe ListUndeliveredOrders do
  subject { ListUndeliveredOrders.new(multi_order_presenter) }

  let(:multi_order_presenter) { double(:multi_order_presenter, :present => nil) }
  let(:order_gateway) { double(:order_gateway, :find => orders) }
  let(:orders) { double(:orders, :to_hash => {}) }

  before do
    allow(subject).to receive(:order_gateway).and_return(order_gateway)
  end

  describe '#execute' do
    it 'looks up the undelivered orders' do
      expect(order_gateway).to receive(:find).with(:delivery_date => nil)
      subject.execute({})
    end

    it 'presents the orders to the preseter as a hash' do
      expect(multi_order_presenter).to receive(:present).with(orders.to_hash)
      subject.execute({})
    end
  end
end