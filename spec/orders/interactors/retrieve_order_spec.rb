require 'spec_helper'

describe RetrieveOrder do
  subject { RetrieveOrder.new(single_order_presenter) }

  let(:order) { double(:order, :to_hash => order_hash) }
  let(:order_hash) { double(:order_hash) }
  let(:single_order_presenter) { double('order presenter', :present => nil) }
  let(:order_gateway) { double('order gateway', :retrieve => order) }

  before do
    allow(subject).to receive(:order_gateway).and_return(order_gateway)
  end

  describe '#get_details' do
    it 'should retrieve the order from the gateway' do
      expect(order_gateway).to receive(:retrieve).with(12)
      subject.get_details(:reference => 12)
    end

    it 'should present the order' do
      expect(single_order_presenter).to receive(:present).with(order_hash)
      subject.get_details(:reference => 12)
    end
  end
end