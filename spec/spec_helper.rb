require 'support/coverage'
require 'rspec'
require 'support/factory_girl'
require 'timecop'
require 'all'

RSpec.configure do |config|
  config.after(:each) do
    Timecop.return
  end
end
