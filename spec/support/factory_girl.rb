require 'factory_girl'

FactoryGirl.find_definitions
FactoryGirl.register_strategy(:create, FactoryGirl::Strategy::Build)
FactoryGirl.define { initialize_with { new(attributes) } }